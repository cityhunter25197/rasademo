# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet
from unidecode import unidecode

import requests
import json
import feedparser
import urllib.request

class ActionHelloWorld(Action):

    def name(self) -> Text:
        return "action_hehe"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text="Hello World!")

        return []

# Ham lay ket qua so xo va tra ve. Ten ham la action_get_lottery
class action_get_lottery(Action):
    def name(self):
            # Doan nay khai bao giong het ten ham ben tren la okie
        return 'action_get_lottery'
    def run(self, dispatcher, tracker, domain):
        # Khai bao dia chi luu tru ket qua so xo. O day lam vi du nen minh lay ket qua SX Mien Bac
        url = 'https://xskt.com.vn/rss-feed/mien-bac-xsmb.rss'
        # Tien hanh lay thong tin tu URL
        feed_cnt = feedparser.parse(url)
        # Lay ket qua so xo moi nhat
        first_node = feed_cnt['entries']
        # Lay thong tin ve ngay va chi tiet cac giai
        return_msg = first_node[0]['title'] + "\n" + first_node[0]['description']
        # Tra ve cho nguoi dung
        dispatcher.utter_message(return_msg)
        return []

# Ham lay gia san pham
class action_get_price(Action):
	def name(self):
            # Doan nay khai bao giong het ten ham ben tren la okie
		    return 'action_get_price'
	def run(self, dispatcher, tracker, domain):
            
            return_msg = tracker.get_slot('cust_price')
            var1 = unidecode(return_msg)
            var_slug = var1.replace(' ', '-')
            data = urllib.request.urlopen("http://localhost/banhang/public/api/api-price/"+var_slug).read()
            output = json.loads(data)
            if len(output) > 0:
                gia = output['promotion_price']
                var3 = "Sản phẩm "+return_msg+" có giá là: "+ "{:,}".format(gia)+" VNĐ. Quý khách có thể xem chi tiết <a href='http://localhost/banhang/public/chi-tiet-san-pham/"+var_slug+"' target='_blank'>Tại Đây</a>"
            else:
                var3 = "Sorry quý khách, shop không có sản phẩm "+return_msg+" ạ"
            dispatcher.utter_message(var3)
            return []

class action_get_newest_product(Action):
	def name(self):
		    return 'action_get_newest_product'
	def run(self, dispatcher, tracker, domain):
            data = urllib.request.urlopen("http://localhost/banhang/public/api/api-newest-product").read()
            output = json.loads(data)
            tensp = output['name']
            var1 = unidecode(tensp)
            var_slug = var1.replace(' ', '-')
            gia = output['promotion_price']
            var3 = "Sản phẩm mới nhất bên em là "+tensp+" có giá là: "+ "{:,}".format(gia)+" VNĐ. Quý khách có thể xem chi tiết <a href='http://localhost/banhang/public/chi-tiet-san-pham/"+var_slug+"' target='_blank'>Tại Đây</a>"
            dispatcher.utter_message(var3)
            return []

class action_get_sale_product(Action):
	def name(self):
		    return 'action_get_sale_product'
	def run(self, dispatcher, tracker, domain):
            data = urllib.request.urlopen("http://localhost/banhang/public/api/api-sale-product").read()
            output = json.loads(data)
            tensp = output['name']
            var1 = unidecode(tensp)
            var_slug = var1.replace(' ', '-')
            gia = output['promotion_price']
            var3 = "Sản phẩm sale nhiều nhất bên em là "+tensp+", có giá: "+ "{:,}".format(gia)+" VNĐ. Quý khách có thể xem chi tiết <a href='http://localhost/banhang/public/chi-tiet-san-pham/"+var_slug+"' target='_blank'>Tại Đây</a>"
            dispatcher.utter_message(var3)
            return []

class action_get_best_seller(Action):
	def name(self):
		    return 'action_get_best_seller'
	def run(self, dispatcher, tracker, domain):
            data = urllib.request.urlopen("http://localhost/banhang/public/api/api-best-seller").read()
            output = json.loads(data)
            tensp = output['name']
            soluong = output['soluong']
            var1 = unidecode(tensp)
            var_slug = var1.replace(' ', '-')
            gia = output['promotion_price']
            var3 = "Sản phẩm bán chạy nhất là "+tensp+", với "+ soluong +" sản phẩm bán ra. Quý khách có thể xem chi tiết <a href='http://localhost/banhang/public/chi-tiet-san-pham/"+var_slug+"' target='_blank'>Tại Đây</a>"
            dispatcher.utter_message(var3)
            return []

class action_ask_acnes(Action):
	def name(self):
		    return 'action_ask_acnes'
	def run(self, dispatcher, tracker, domain):
            return_msg = tracker.get_slot('cust_gender')
            var = unidecode(return_msg)
            if var.lower() == "nam":
                var2 = "Sản phẩm trị mụn hiệu quả cho NAM là Kem Trị Mụn Flower. Quý khách có thể xem chi tiết <a href='http://localhost/banhang/public/chi-tiet-san-pham/kem-tri-mun-flower' target='_blank'>Tại Đây</a>"
            else:
                var2 = "Sản phẩm trị mụn hiệu quả cho NỮ là Kem Trị Mụn Eucerin. Quý khách có thể xem chi tiết <a href='http://localhost/banhang/public/chi-tiet-san-pham/kem-tri-mun-eucerin' target='_blank'>Tại Đây</a>"
            dispatcher.utter_message(var2)
            return []

class action_ask_product_by_price(Action):
	def name(self):
		    return 'action_ask_product_by_price'
	def run(self, dispatcher, tracker, domain):
            cust_category = tracker.get_slot('cust_category')
            cust_category_price = tracker.get_slot('cust_category_price')
            var1 = unidecode(cust_category)
            var_slug_category = var1.replace(' ', '-')
            data = urllib.request.urlopen("http://localhost/banhang/public/api/api-list-category/"+var_slug_category).read()
            outputs = json.loads(data)
            tien = quydoi(cust_category_price)
            num = 0
            for output in outputs:
                if int(output['promotion_price']) <= tien:
                    break
                num = num + 1
            if num >= len(outputs):
                var3 = "Sorry quý khách, với tầm giá "+cust_category_price+", shop không có sản phẩm "+cust_category+" nào phù hợp ạ."
            else:
                name = outputs[num]['name']
                var2 = unidecode(name)
                var_slug_product = var2.replace(' ', '-')
                var3 = "Với tầm giá "+cust_category_price+" quý khách có thể tham khảo sản phẩm "+name+", xem chi tiết <a href='http://localhost/banhang/public/chi-tiet-san-pham/"+var_slug_product+"' target='_blank'>Tại Đây</a>"
            dispatcher.utter_message(var3)
            return []

def quydoi(tien):
    if tien == "100 ngàn":
        return 100000
    elif tien == "150 ngàn":
        return 150000
    elif tien == "200 ngàn":
        return 200000
    elif tien == "250 ngàn":
        return 250000
    elif tien == "300 ngàn":
        return 300000
    elif tien == "350 ngàn":
        return 350000
    elif tien == "400 ngàn":
        return 400000
    elif tien == "450 ngàn":
        return 450000
    elif tien == "500 ngàn":
        return 500000
    elif tien == "550 ngàn":
        return 550000
    elif tien == "600 ngàn":
        return 600000
    elif tien == "650 ngàn":
        return 650000
    elif tien == "700 ngàn":
        return 700000
    elif tien == "750 ngàn":
        return 750000
    elif tien == "800 ngàn":
        return 800000

class action_ask_best_product_in_category(Action):
	def name(self):
		    return 'action_ask_best_product_in_category'
	def run(self, dispatcher, tracker, domain):
            cust_category_best_product = tracker.get_slot('cust_category')
            var1 = unidecode(cust_category_best_product)
            var_slug_category = var1.replace(' ', '-')
            data = urllib.request.urlopen("http://localhost/banhang/public/api/api-cheapest-promotion-price-product-in-category/"+var_slug_category).read()
            output = json.loads(data)
            var_ten = output["name"]
            var_slug_product = output["slug"]
            gia = output["promotion_price"]
            var3 = "Loại "+cust_category_best_product+" rẻ nhất bên em là "+var_ten+", có giá là "+"{:,}".format(gia)+"đ. Mua hàng <a href='http://localhost/banhang/public/chi-tiet-san-pham/"+var_slug_product+"' target='_blank'>Tại Đây</a>"
            dispatcher.utter_message(var3)
            return []

class action_ask_product_of_category(Action):
	def name(self):
		    return 'action_ask_product_of_category'
	def run(self, dispatcher, tracker, domain):
            cust_category = tracker.get_slot('cust_category')
            var1 = unidecode(cust_category)
            var_slug_category = var1.replace(' ', '-')
            data = urllib.request.urlopen("http://localhost/banhang/public/api/api-list-product-of-category/"+var_slug_category).read()
            outputs = json.loads(data)
            var3 = "Một số loại "+cust_category+" mới nhất bên em: <br>"
            for output in outputs:
                gia = output['promotion_price']
                var_name_product = output['name']
                var_slug_product = output['slug']
                var3 = var3 + "- <a href='http://localhost/banhang/public/chi-tiet-san-pham/"+var_slug_product+"' target='_blank'>"+var_name_product+"</a>, giá "+"{:,}".format(gia)+"đ<br>"
            dispatcher.utter_message(var3)
            return []

class action_ask_product_of_brand(Action):
	def name(self):
		    return 'action_ask_product_of_brand'
	def run(self, dispatcher, tracker, domain):
            cust_brand = tracker.get_slot('cust_brand')
            var1 = unidecode(cust_brand)
            var_slug_brand = var1.replace(' ', '-')
            data = urllib.request.urlopen("http://localhost/banhang/public/api/api-list-product-of-brand/"+var_slug_brand).read()
            outputs = json.loads(data)
            var3 = "Thương hiệu "+cust_brand+" có các sản phẩm như sau: <br>"
            for output in outputs:
                gia = output['promotion_price']
                var_name_product = output['name']
                var_slug_product = output['slug']
                var3 = var3 + "- <a href='http://localhost/banhang/public/chi-tiet-san-pham/"+var_slug_product+"' target='_blank'>"+var_name_product+"</a>, giá "+"{:,}".format(gia)+"đ<br>"
            dispatcher.utter_message(var3)
            return []

class action_ask_promotion_product_in_category(Action):
	def name(self):
		    return 'action_ask_promotion_product_in_category'
	def run(self, dispatcher, tracker, domain):
            cust_category_promotion_product = tracker.get_slot('cust_category')
            var1 = unidecode(cust_category_promotion_product)
            var_slug_category = var1.replace(' ', '-')
            data = urllib.request.urlopen("http://localhost/banhang/public/api/api-promotion-product-in-category/"+var_slug_category).read()
            outputs = json.loads(data)
            if len(outputs) > 0:
                var3 = "Các sản phẩm "+cust_category_promotion_product+" đang khuyến mãi là: <br>"
                for output in outputs:
                    gia = output['promotion_price']
                    var_name_product = output['name']
                    var_slug_product = output['slug']
                    var3 = var3 + "- <a href='http://localhost/banhang/public/chi-tiet-san-pham/"+var_slug_product+"' target='_blank'>"+var_name_product+"</a>, giá "+"{:,}".format(gia)+"đ<br>"
            else:
                var3 = "Sorry quý khách, hiện tại shop không có loại "+cust_category_promotion_product+" nào đang khuyến mãi ạ."
            dispatcher.utter_message(var3)
            return []

class action_ask_best_seller_in_category(Action):
	def name(self):
		    return 'action_ask_best_seller_in_category'
	def run(self, dispatcher, tracker, domain):
            cust_category = tracker.get_slot('cust_category')
            var1 = unidecode(cust_category)
            var_slug_category = var1.replace(' ', '-')
            data = urllib.request.urlopen("http://localhost/banhang/public/api/api-best-seller-in-category/"+var_slug_category).read()
            output = json.loads(data)
            tensp = output['name']
            soluong = output['soluong']
            var1 = unidecode(tensp)
            var_slug = var1.replace(' ', '-')
            gia = output['promotion_price']
            var3 = "Loại "+cust_category+" bán chạy nhất là "+tensp+", với "+ soluong +" sản phẩm bán ra. Quý khách có thể xem chi tiết <a href='http://localhost/banhang/public/chi-tiet-san-pham/"+var_slug+"' target='_blank'>Tại Đây</a>"
            dispatcher.utter_message(var3)
            return []