## happy path
* greet
  - utter_greet
* mood_great
  - utter_happy

## goodbye
* greet
  - utter_greet
* goodbye
  - utter_goodbye
## goodbye 2
* goodbye
  - utter_goodbye

## bot challenge
* bot_challenge
  - utter_iamabot

## Chào hỏi và hỏi kết quả hehe
* greet
  - utter_greet
* ask_hehe
  - action_hehe

## Chào hỏi và hỏi kết quả sổ xố
* greet
  - utter_greet
* ask_lottery
  - action_get_lottery

## Chào hỏi và hỏi giá
* ask_price
  - action_get_price

## Chào hỏi và hỏi sản phẩm mới nhất
* ask_newest_product
  - action_get_newest_product

## Chào hỏi và hỏi sản phẩm sale mạnh nhất
* ask_sale_product
  - action_get_sale_product

## hỏi sản phẩm bán chạy nhất
* ask_best_seller
  - action_get_best_seller

## story get_started
* get_started
  - utter_get_started

## story out_of_scopre
* out_of_scope
  - utter_out_of_scope

## hỏi kem trị mụn
* ask_acnes
  - utter_ask_gender
* give_gender
  - action_ask_acnes

## hỏi tìm loại sản phẩm theo giá
* ask_product_by_price
  - action_ask_product_by_price

## hỏi tìm sản phẩm rẻ nhất theo loại
* ask_best_product_in_category
  - action_ask_best_product_in_category

## cảm ơn
* thanks
  - utter_thanks

## hỏi về phương thức thanh toán + banking
* ask_payment
  - utter_payment
* ask_banking
  - utter_banking

## hỏi về phương thức thanh toán + shipping
* ask_payment
  - utter_payment
* ask_ship_fee
  - utter_ship_fee

# hỏi về phí ship + time ship
* ask_ship_fee
  - utter_ship_fee
* ask_time_shipping
  - utter_time_shipping

# hỏi về time ship
* ask_time_shipping
  - utter_time_shipping

# hỏi về time làm việc
* ask_time_work
  - utter_time_work

# hỏi tìm loại sản phẩm theo giá 2
* ask_product_by_category
  - utter_ask_budget
* ask_product_by_price_after_category
  - action_ask_product_by_price

# hỏi sản phẩm trong từng loại
* ask_list_product_of_category
  - action_ask_product_of_category

# hỏi sản phẩm trong từng brand
* ask_list_product_of_brand
  - action_ask_product_of_brand

# hỏi sẹo lồi
* ask_keloids
  - utter_ask_keloids

# hỏi tóc khô xơ
* ask_dry_hair
  - utter_ask_dry_hair

# hỏi da dị ứng
* ask_sensitive_skin
  - utter_ask_sensitive_skin

# hỏi tìm sản phẩm khuyến mãi theo loại
* ask_promotion_product_in_category
  - action_ask_promotion_product_in_category

# hỏi tìm sản phẩm bán chạy nhất theo loại
* ask_best_seller_in_category
  - action_ask_best_seller_in_category