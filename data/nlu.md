## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there
- Xin chào
- chào buổi sáng
- chào buổi chiều
- chào

## intent:thanks
- thank
- thanks
- cảm ơn
- thank you

## intent:goodbye
- bye
- goodbye
- see you around
- see you later
- tạm biệt
- bai

## intent:affirm
- yes
- indeed
- of course
- that sounds good
- correct

## intent:deny
- no
- never
- I don't think so
- don't like that
- no way
- not really

## intent:mood_great
- perfect
- very good
- great
- amazing
- wonderful
- I am feeling very good
- I am great
- I'm good

## intent:mood_unhappy
- sad
- very sad
- unhappy
- bad
- very bad
- awful
- terrible
- not very good
- extremely sad
- so sad
- tôi buồn
- chán quá

## intent:bot_challenge
- are you a bot?
- are you a human?
- am I talking to a bot?
- am I talking to a human?
- Bạn là ai?

## intent:ask_hehe
- hehe

## intent:ask_lottery
- Cho anh xem kết quả đê
- Kết quả số xố hôm này thế nào
- Mày cho anh hỏi kết quả tý
- Có kết quả chưa

## intent:ask_price
- [kem trị mụn feck](cust_price) giá bao nhiêu
- [kem trị mụn flower](cust_price) giá bao nhiêu
- [kem trị mụn eucerin](cust_price) giá bao nhiêu
- [kem trị mụn flow](cust_price) giá bao nhiêu
- [son môi tpa](cust_price) giá bao nhiêu
- [son môi bóng hồng](cust_price) giá bao nhiêu
- [trị sẹo rejuvasil](cust_price) giá bao nhiêu
- [sửa rửa mặt cream](cust_price) giá bao nhiêu
- [sửa rửa mặt menly](cust_price) giá bao nhiêu
- [dưỡng tóc charme](cust_price) giá bao nhiêu
- [nước hoa ruby sport](cust_price) giá bao nhiêu
- [nước hoa enternity](cust_price) giá bao nhiêu
- [nước hoa charme iris](cust_price) giá bao nhiêu
- [nước hoa bleu](cust_price) giá bao nhiêu
- [gen trị mụn menly](cust_price) giá bao nhiêu
- [kem dưỡng cerave](cust_price) giá bao nhiêu
- [cọ má janny](cust_price) giá bao nhiêu
- [chuốt mi missha](cust_price) giá bao nhiêu
- [xăm môi](cust_price) giá bao nhiêu
- giá của [kem trị mụn feck](cust_price)
- giá của [kem trị mụn flower](cust_price)
- giá của [kem trị mụn eucerin](cust_price)
- giá của [kem trị mụn flow](cust_price)
- giá của [son môi tpa](cust_price)
- giá của [son môi bóng hồng](cust_price)
- giá của [trị sẹo rejuvasil](cust_price)
- giá của [sửa rửa mặt cream](cust_price)
- giá của [sửa rửa mặt menly](cust_price)
- giá của [dưỡng tóc charme](cust_price)
- giá của [nước hoa ruby sport](cust_price)
- giá của [nước hoa enternity](cust_price)
- giá của [nước hoa charme iris](cust_price)
- giá của [nước hoa bleu](cust_price)
- giá của [gen trị mụn menly](cust_price)
- giá của [kem dưỡng cerave](cust_price)
- giá của [cọ má janny](cust_price)
- giá của [chuốt mi missha](cust_price)
- giá của [xăm môi](cust_price)

## intent:ask_newest_product
- sản phẩm nào mới nhất
- shop có sản phẩm gì mới không
- có gì mới không

## intent:ask_sale_product
- sản phẩm nào giảm giá nhiều nhất
- thứ gì sale mạnh nhất
- sản phẩm sale kịch sàn
- đang sale cái gì thế

## intent:get_started
- tôi muốn nói chuyện
- I want to talk

## intent:out_of_scope
- I want pizza
- I hate you
- Tôi muốn pizza
- Tôi ghét bạn

## intent:ask_best_seller
- sản phẩm bán chạy nhất
- shop có sản phẩm gì hot không
- thứ gì được mua nhiều nhất
- sản phẩm bán nhiều nhất
- sản phẩm được mua nhiều

## intent:ask_acnes
- tôi bị mụn thì phải dùng gì
- dùng gì để hết mụn
- sản phẩm nào trị mụn
- mụn nhiều phải dùng gì
- bị mụn nhiều
- bị mụn
- làm sao để hết mụn

## intent:give_gender
- [Nam](cust_gender)
- [Nữ](cust_gender)
- [nam](cust_gender)
- [nữ](cust_gender)
- [nu](cust_gender)
- tôi là [nam](cust_gender)
- tôi là [nữ](cust_gender)
- tôi là [nu](cust_gender)

## intent:ask_product_by_price
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [100 ngàn](cust_category_price)
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [150 ngàn](cust_category_price)
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [200 ngàn](cust_category_price)
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [250 ngàn](cust_category_price)
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [300 ngàn](cust_category_price)
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [350 ngàn](cust_category_price)
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [400 ngàn](cust_category_price)
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [450 ngàn](cust_category_price)
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [500 ngàn](cust_category_price)
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [550 ngàn](cust_category_price)
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [600 ngàn](cust_category_price)
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [650 ngàn](cust_category_price)
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [700 ngàn](cust_category_price)
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [750 ngàn](cust_category_price)
- tôi muốn mua một loại [son môi](cust_category) có giá khoảng [800 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [100 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [150 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [200 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [250 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [300 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [350 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [400 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [450 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [500 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [550 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [600 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [650 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [700 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [750 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nam](cust_category) có giá khoảng [800 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [100 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [150 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [200 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [250 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [300 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [350 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [400 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [450 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [500 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [550 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [600 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [650 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [700 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [750 ngàn](cust_category_price)
- tôi muốn mua một loại [nước hoa cho nam](cust_category) có giá khoảng [800 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [100 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [150 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [200 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [250 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [300 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [350 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [400 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [450 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [500 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [550 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [600 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [650 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [700 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [750 ngàn](cust_category_price)
- tôi muốn mua một loại [sửa rửa mặt cho nam](cust_category) có giá khoảng [800 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [100 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [150 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [200 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [250 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [300 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [350 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [400 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [450 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [500 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [550 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [600 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [650 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [700 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [750 ngàn](cust_category_price)
- tôi muốn mua một loại [kem dưỡng ẩm](cust_category) có giá khoảng [800 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [100 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [150 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [200 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [250 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [300 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [350 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [400 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [450 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [500 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [550 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [600 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [650 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [700 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [750 ngàn](cust_category_price)
- tôi muốn mua một loại [kem trị mụn cho nữ](cust_category) có giá khoảng [800 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [100 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [150 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [200 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [250 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [300 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [350 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [400 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [450 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [500 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [550 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [600 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [650 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [700 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [750 ngàn](cust_category_price)
- tôi muốn mua một loại [thuốc trị sẹo cho nam](cust_category) có giá khoảng [800 ngàn](cust_category_price)
- có [100 ngàn](cust_category_price) thì mua loại [son môi](cust_category) gì
- có [200 ngàn](cust_category_price) thì mua loại [son môi](cust_category) gì
- có [300 ngàn](cust_category_price) thì mua loại [son môi](cust_category) gì
- có [400 ngàn](cust_category_price) thì mua loại [son môi](cust_category) gì
- có [500 ngàn](cust_category_price) thì mua loại [son môi](cust_category) gì
- có [600 ngàn](cust_category_price) thì mua loại [son môi](cust_category) gì
- có [700 ngàn](cust_category_price) thì mua loại [son môi](cust_category) gì
- có [800 ngàn](cust_category_price) thì mua loại [son môi](cust_category) gì
- có [100 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nam](cust_category) gì
- có [200 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nam](cust_category) gì
- có [300 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nam](cust_category) gì
- có [400 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nam](cust_category) gì
- có [500 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nam](cust_category) gì
- có [600 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nam](cust_category) gì
- có [700 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nam](cust_category) gì
- có [800 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nam](cust_category) gì
- có [100 ngàn](cust_category_price) thì mua loại [nước hoa cho nam](cust_category) gì
- có [200 ngàn](cust_category_price) thì mua loại [nước hoa cho nam](cust_category) gì
- có [300 ngàn](cust_category_price) thì mua loại [nước hoa cho nam](cust_category) gì
- có [400 ngàn](cust_category_price) thì mua loại [nước hoa cho nam](cust_category) gì
- có [500 ngàn](cust_category_price) thì mua loại [nước hoa cho nam](cust_category) gì
- có [600 ngàn](cust_category_price) thì mua loại [nước hoa cho nam](cust_category) gì
- có [700 ngàn](cust_category_price) thì mua loại [nước hoa cho nam](cust_category) gì
- có [800 ngàn](cust_category_price) thì mua loại [nước hoa cho nam](cust_category) gì
- có [100 ngàn](cust_category_price) thì mua loại [sửa rửa mặt cho nam](cust_category) gì
- có [200 ngàn](cust_category_price) thì mua loại [sửa rửa mặt cho nam](cust_category) gì
- có [300 ngàn](cust_category_price) thì mua loại [sửa rửa mặt cho nam](cust_category) gì
- có [400 ngàn](cust_category_price) thì mua loại [sửa rửa mặt cho nam](cust_category) gì
- có [500 ngàn](cust_category_price) thì mua loại [sửa rửa mặt cho nam](cust_category) gì
- có [600 ngàn](cust_category_price) thì mua loại [sửa rửa mặt cho nam](cust_category) gì
- có [700 ngàn](cust_category_price) thì mua loại [sửa rửa mặt cho nam](cust_category) gì
- có [800 ngàn](cust_category_price) thì mua loại [sửa rửa mặt cho nam](cust_category) gì
- có [100 ngàn](cust_category_price) thì mua loại [kem dưỡng ẩm](cust_category) gì
- có [200 ngàn](cust_category_price) thì mua loại [kem dưỡng ẩm](cust_category) gì
- có [300 ngàn](cust_category_price) thì mua loại [kem dưỡng ẩm](cust_category) gì
- có [400 ngàn](cust_category_price) thì mua loại [kem dưỡng ẩm](cust_category) gì
- có [500 ngàn](cust_category_price) thì mua loại [kem dưỡng ẩm](cust_category) gì
- có [600 ngàn](cust_category_price) thì mua loại [kem dưỡng ẩm](cust_category) gì
- có [700 ngàn](cust_category_price) thì mua loại [kem dưỡng ẩm](cust_category) gì
- có [800 ngàn](cust_category_price) thì mua loại [kem dưỡng ẩm](cust_category) gì
- có [100 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nữ](cust_category) gì
- có [200 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nữ](cust_category) gì
- có [300 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nữ](cust_category) gì
- có [400 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nữ](cust_category) gì
- có [500 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nữ](cust_category) gì
- có [600 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nữ](cust_category) gì
- có [700 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nữ](cust_category) gì
- có [800 ngàn](cust_category_price) thì mua loại [kem trị mụn cho nữ](cust_category) gì
- có [100 ngàn](cust_category_price) thì mua loại [thuốc trị sẹo cho nam](cust_category) gì
- có [200 ngàn](cust_category_price) thì mua loại [thuốc trị sẹo cho nam](cust_category) gì
- có [300 ngàn](cust_category_price) thì mua loại [thuốc trị sẹo cho nam](cust_category) gì
- có [400 ngàn](cust_category_price) thì mua loại [thuốc trị sẹo cho nam](cust_category) gì
- có [500 ngàn](cust_category_price) thì mua loại [thuốc trị sẹo cho nam](cust_category) gì
- có [600 ngàn](cust_category_price) thì mua loại [thuốc trị sẹo cho nam](cust_category) gì
- có [700 ngàn](cust_category_price) thì mua loại [thuốc trị sẹo cho nam](cust_category) gì
- có [800 ngàn](cust_category_price) thì mua loại [thuốc trị sẹo cho nam](cust_category) gì

## intent:ask_payment
- Tôi có thể thanh toán bằng cách nào?
- Hình thức thanh toán thế nào?
- trả tiền như thế nào
- hình thức thanh toán

## intent:ask_time_shipping
- Từ khi tôi mua hàng đến khi nhận hàng là bao lâu?
- bao lâu thì tôi có thể nhận hàng
- tôi phải đợi bao lâu để nhận hàng
- thời gian ship

## intent:ask_time_work
- Shop có làm việc thứ bảy không
- thời gian làm việc của shop
- thời gian làm
- nghỉ cuối tuần không

## intent:ask_ship_fee
- phí ship là bao nhiêu
- tiền ship

## intent:ask_banking
- ngân hàng nào làm việc
- tài khoản ngân hàng nào
- ngân hàng chi

## intent:ask_product_by_category
- tôi muốn mua [son môi](cust_category)
- tôi muốn mua [kem dưỡng ẩm](cust_category)
- tôi muốn mua [kem trị mụn cho nữ](cust_category)
- tôi muốn mua [kem trị mụn cho nam](cust_category)
- tôi muốn mua [kem thuốc trị sẹo cho nam](cust_category)
- tôi muốn mua [sửa rửa mặt cho nam](cust_category)
- tôi muốn mua [nước hoa cho nam](cust_category)

## intent:ask_product_by_price_after_category
- tầm [100 ngàn](cust_category_price)
- tầm [150 ngàn](cust_category_price)
- tầm [200 ngàn](cust_category_price)
- tầm [250 ngàn](cust_category_price)
- tầm [300 ngàn](cust_category_price)
- tầm [350 ngàn](cust_category_price)
- tầm [400 ngàn](cust_category_price)
- tầm [450 ngàn](cust_category_price)
- tầm [500 ngàn](cust_category_price)
- tầm [550 ngàn](cust_category_price)
- tầm [600 ngàn](cust_category_price)
- tầm [650 ngàn](cust_category_price)
- tầm [700 ngàn](cust_category_price)
- tầm [750 ngàn](cust_category_price)
- tầm [800 ngàn](cust_category_price)
- [100 ngàn](cust_category_price)
- [150 ngàn](cust_category_price)
- [200 ngàn](cust_category_price)
- [250 ngàn](cust_category_price)
- [300 ngàn](cust_category_price)
- [350 ngàn](cust_category_price)
- [400 ngàn](cust_category_price)
- [450 ngàn](cust_category_price)
- [500 ngàn](cust_category_price)
- [550 ngàn](cust_category_price)
- [600 ngàn](cust_category_price)
- [650 ngàn](cust_category_price)
- [700 ngàn](cust_category_price)
- [750 ngàn](cust_category_price)
- [800 ngàn](cust_category_price)

## intent:ask_list_product_of_category
- liệt kê các loại [son môi](cust_category)
- liệt kê các loại [trang điểm](cust_category)
- liệt kê các loại [dưỡng tóc](cust_category)
- liệt kê các loại [kem trị mụn cho nam](cust_category)
- liệt kê các loại [kem dưỡng ẩm](cust_category)
- liệt kê các loại [kem trị mụn cho nữ](cust_category)
- liệt kê các loại [thuốc trị sẹo cho nam](cust_category)
- liệt kê các loại [sửa rửa mặt cho nam](cust_category)
- liệt kê các loại [nước hoa cho nam](cust_category)
- shop có loại [son môi](cust_category) nào
- shop có loại [trang điểm](cust_category) nào
- shop có loại [dưỡng tóc](cust_category) nào
- shop có loại [kem trị mụn cho nam](cust_category) nào
- shop có loại [kem dưỡng ẩm](cust_category) nào
- shop có loại [kem trị mụn cho nữ](cust_category) nào
- shop có loại [thuốc trị sẹo cho nam](cust_category) nào
- shop có loại [sửa rửa mặt cho nam](cust_category) nào
- shop có loại [nước hoa cho nam](cust_category) nào
- loại [son môi](cust_category) nào hot nhất
- loại [dưỡng tóc](cust_category) nào hot nhất
- loại [kem trị mụn cho nam](cust_category) nào hot nhất
- loại [nước hoa cho nam](cust_category) nào hot nhất
- loại [kem dưỡng ẩm](cust_category) nào hot nhất
- loại [kem trị mụn cho nữ](cust_category) nào hot nhất
- loại [thuốc trị sẹo cho nam](cust_category) nào hot nhất
- loại [sửa rửa mặt cho nam](cust_category) nào hot nhất
- loại [son môi](cust_category) nào mới nhất
- loại [dưỡng tóc](cust_category) nào mới nhất
- loại [kem trị mụn cho nam](cust_category) nào mới nhất
- loại [nước hoa cho nam](cust_category) nào mới nhất
- loại [kem dưỡng ẩm](cust_category) nào mới nhất
- loại [kem trị mụn cho nữ](cust_category) nào mới nhất
- loại [thuốc trị sẹo cho nam](cust_category) nào mới nhất
- loại [sửa rửa mặt cho nam](cust_category) nào mới nhất

## intent:ask_best_product_in_category
- loại [son môi](cust_category) nào rẻ nhất
- loại [dưỡng tóc](cust_category) nào rẻ nhất
- loại [kem trị mụn cho nam](cust_category) nào rẻ nhất
- loại [nước hoa cho nam](cust_category) nào rẻ nhất
- loại [kem dưỡng ẩm](cust_category) nào rẻ nhất
- loại [kem trị mụn cho nữ](cust_category) nào rẻ nhất
- loại [thuốc trị sẹo cho nam](cust_category) nào rẻ nhất
- loại [sửa rửa mặt cho nam](cust_category) nào rẻ nhất
- sản phẩm [son môi](cust_category) nào vừa tốt vừa rẻ
- sản phẩm [kem trị mụn cho nam](cust_category) nào vừa tốt vừa rẻ
- sản phẩm [nước hoa cho nam](cust_category) nào vừa tốt vừa rẻ
- sản phẩm [kem dưỡng ẩm](cust_category) nào vừa tốt vừa rẻ
- sản phẩm [kem trị mụn cho nữ](cust_category) nào vừa tốt vừa rẻ
- sản phẩm [thuốc trị sẹo cho nam](cust_category) nào vừa tốt vừa rẻ
- sản phẩm [sửa rửa mặt cho nam](cust_category) nào vừa tốt vừa rẻ

## intent:ask_list_product_of_brand
- các sản phẩm của thương hiệu [menly](cust_brand)
- các sản phẩm của thương hiệu [adventure](cust_brand)
- các sản phẩm của thương hiệu [dubby](cust_brand)
- các sản phẩm của thương hiệu [matt](cust_brand)
- các sản phẩm của thương hiệu [handcrafted](cust_brand)
- thương hiệu [menly](cust_brand) có những gì
- thương hiệu [adventure](cust_brand) có những gì
- thương hiệu [dubby](cust_brand) có những gì
- thương hiệu [matt](cust_brand) có những gì
- thương hiệu [handcrafted](cust_brand) có những gì

## intent:ask_keloids
- trị sẹo lồi tốt nhất
- tui bị sẹo lồi
- làm sao để hết sẹo lồi
- sẹo lồi nên dùng gì đây

## intent:ask_dry_hair
- tóc tui bị khô xơ
- tóc khô xơ nên dùng loại nào
- sản phẩm nào dùng cho tóc xơ
- tóc xơ
- khô xơ da đầu

## intent:ask_promotion_product_in_category
- các sản phẩm [son môi](cust_category) đang khuyến mãi
- các sản phẩm [kem trị mụn cho nam](cust_category) đang khuyến mãi
- các sản phẩm [sửa rửa mặt cho nam](cust_category) đang khuyến mãi
- các sản phẩm [dưỡng tóc](cust_category) đang khuyến mãi
- các sản phẩm [nước hoa cho nam](cust_category) đang khuyến mãi
- các sản phẩm [kem dưỡng ẩm](cust_category) đang khuyến mãi
- các sản phẩm [kem trị mụn cho nữ](cust_category) đang khuyến mãi
- các sản phẩm [thuốc trị sẹo cho nam](cust_category) đang khuyến mãi
- các sản phẩm [trang điểm](cust_category) đang khuyến mãi

## intent:ask_best_seller_in_category
- sản phẩm [son môi](cust_category) bán chạy nhất
- sản phẩm [kem trị mụn cho nam](cust_category) bán chạy nhất
- sản phẩm [sửa rửa mặt cho nam](cust_category) bán chạy nhất
- sản phẩm [dưỡng tóc](cust_category) bán chạy nhất
- sản phẩm [nước hoa cho nam](cust_category) bán chạy nhất
- sản phẩm [kem dưỡng ẩm](cust_category) bán chạy nhất
- sản phẩm [kem trị mụn cho nữ](cust_category) bán chạy nhất
- sản phẩm [thuốc trị sẹo cho nam](cust_category) bán chạy nhất
- sản phẩm [trang điểm](cust_category) bán chạy nhất
- loại [son môi](cust_category) nào được mua nhiều nhất
- loại [kem trị mụn cho nam](cust_category) nào được mua nhiều nhất
- loại [sửa rửa mặt cho nam](cust_category) nào được mua nhiều nhất
- loại [dưỡng tóc](cust_category) nào được mua nhiều nhất
- loại [nước hoa cho nam](cust_category) nào được mua nhiều nhất
- loại [kem dưỡng ẩm](cust_category) nào được mua nhiều nhất
- loại [kem trị mụn cho nữ](cust_category) nào được mua nhiều nhất
- loại [thuốc trị sẹo cho nam](cust_category) nào được mua nhiều nhất
- loại [trang điểm](cust_category) nào được mua nhiều nhất

## intent:ask_sensitive_skin
- da bị nhạy cảm thì dùng kem dưỡng gì
- loại kem dưỡng ẩm nào dùng cho da nhạy cảm
- da tôi bị nhạy cảm nên dùng loại gì
- tôi bị dị ứng thì dùng loại kem dưỡng ẩm nào
- loại kem dưỡng ẩm cho da dị ứng
